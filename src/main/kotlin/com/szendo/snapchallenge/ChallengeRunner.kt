package com.szendo.snapchallenge

import com.szendo.snapchallenge.api.ChallengeApi
import com.szendo.snapchallenge.api.model.*
import com.szendo.snapchallenge.solver.Problem
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.encodeToJsonElement

class ChallengeRunner(apiToken: String) {
    val challengeApi: ChallengeApi

    init {
        challengeApi = ChallengeApi(apiToken)
    }

    inline fun <reified In, reified Out> run(sampleIndex: Int? = null, problem: Problem<In, Out>): Boolean {
        val (id, testCount) = challengeApi.startSubmission(StartSubmissionRequest(problem.problemId, sampleIndex))
        repeat(testCount) { testIndex ->
            val (testId, _, input) = challengeApi.startTest(StartTestRequest(id))
            println("Running test #$testIndex ($testId), input: $input")
            val output = Json.encodeToJsonElement(problem.solve(Json.decodeFromJsonElement(input)))
            println("Submitting output: $output")
            val (correct) = challengeApi.submitTestResult(SubmitTestResultRequest(output), testId)
            if (!correct) {
                println("Incorrect answer")
                return false
            }
        }
        return true
    }
}
