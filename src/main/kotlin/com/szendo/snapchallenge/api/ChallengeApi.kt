package com.szendo.snapchallenge.api

import com.szendo.snapchallenge.api.model.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response

class ChallengeApi(private val apiToken: String, private val apiUrl: String = "https://challenge.snapsoft.hu") {
    private val httpClient = OkHttpClient.Builder()
        .addInterceptor { chain ->
            chain.proceed(chain.request().newBuilder().addHeader("X-Api-Token", apiToken).build())
        }
        .build()

    private val jsonMediaType = "application/json".toMediaType()

    private inline fun <reified T> T.toJsonRequestBody() = Json.encodeToString(this).toRequestBody(jsonMediaType)

    @OptIn(ExperimentalSerializationApi::class)
    private inline fun <reified T> Response.extractResponse() =
        if (isSuccessful) Json.decodeFromStream<T>(body!!.byteStream())
        else error("Unsuccessful call: ${request.method} ${request.url}, status: $code $message), body: ${body?.string()}")

    fun startSubmission(request: StartSubmissionRequest): Submission = httpClient.newCall(
        Request.Builder()
            .url("$apiUrl/api/submissions/start-submission")
            .post(request.toJsonRequestBody())
            .build()
    ).execute().extractResponse<StartSubmissionResponse>().submission

    fun startTest(request: StartTestRequest): Test = httpClient.newCall(
        Request.Builder()
            .url("$apiUrl/api/submissions/test")
            .put(request.toJsonRequestBody())
            .build()
    ).execute().extractResponse()

    fun submitTestResult(request: SubmitTestResultRequest, testId: String): TestResult = httpClient.newCall(
        Request.Builder()
            .url("$apiUrl/api/submissions/test/$testId")
            .post(request.toJsonRequestBody())
            .build()
    ).execute().extractResponse()
}
