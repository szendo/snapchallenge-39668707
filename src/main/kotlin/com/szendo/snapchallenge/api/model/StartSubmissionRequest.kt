package com.szendo.snapchallenge.api.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class StartSubmissionRequest(
    val problem: String,
    @SerialName("sample_index") val sampleIndex: Int? = null,
)
