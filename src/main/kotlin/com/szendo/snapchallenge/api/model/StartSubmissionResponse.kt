package com.szendo.snapchallenge.api.model

import kotlinx.serialization.Serializable

@Serializable
data class StartSubmissionResponse(
    val submission: Submission,
)
