package com.szendo.snapchallenge.api.model

import kotlinx.serialization.Serializable

@Serializable
data class StartTestRequest(
    val submission: String,
)
