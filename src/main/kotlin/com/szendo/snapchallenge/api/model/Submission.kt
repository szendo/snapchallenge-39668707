package com.szendo.snapchallenge.api.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Submission(
    val id: String,
    @SerialName("test_count") val testCount: Int,
    @SerialName("sample_index") val sampleIndex: Int? = null,
    @SerialName("started_at") val startedAt: Long,
)
