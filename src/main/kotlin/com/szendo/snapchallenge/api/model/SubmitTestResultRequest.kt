package com.szendo.snapchallenge.api.model

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
data class SubmitTestResultRequest(
    val output: JsonElement,
)
