package com.szendo.snapchallenge.api.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
data class Test(
    @SerialName("test_id") val testId: String,
    val deadline: Long,
    val input: JsonElement,
)
