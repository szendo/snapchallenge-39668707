package com.szendo.snapchallenge.api.model

import kotlinx.serialization.Serializable

@Serializable
data class TestResult(
    val correct: Boolean,
)
