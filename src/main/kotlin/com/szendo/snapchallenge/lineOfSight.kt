package com.szendo.snapchallenge

import com.szendo.snapchallenge.solver.LineOfSightProblem

fun main() {
    ChallengeRunner(System.getenv("API_TOKEN")).run(sampleIndex = 0, LineOfSightProblem)
}
