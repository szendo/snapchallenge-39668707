package com.szendo.snapchallenge

import com.szendo.snapchallenge.solver.SanityCheckProblem

fun main() {
    ChallengeRunner(System.getenv("API_TOKEN")).run(sampleIndex = 0, SanityCheckProblem)
}
