package com.szendo.snapchallenge

import com.szendo.snapchallenge.solver.SawSquareyProblem

fun main() {
    ChallengeRunner(System.getenv("API_TOKEN")).run(sampleIndex = 0, SawSquareyProblem)
}
