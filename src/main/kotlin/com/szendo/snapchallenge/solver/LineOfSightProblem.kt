package com.szendo.snapchallenge.solver

import kotlinx.serialization.Serializable

object LineOfSightProblem : Problem<LineOfSightInput, LosCoord>("line-of-sight") {
    override fun solve(input: LineOfSightInput): LosCoord {
        var coord = 0 to 0
        var clearFields = 0L

        repeat(input.boardSize) { y ->
            repeat(input.boardSize) { x ->
                val c = countClearFields(input, x to y)
                if (c > clearFields) {
                    clearFields = c
                    coord = x to y
                }
            }

        }

        return LosCoord(coord.x, coord.y)
    }

    private fun countClearFields(input: LineOfSightInput, coord: Coord): Long {
        return if (LosCoord(coord.x, coord.y) in input.obstacles) 0L
        else listOf(-1 to -1, -1 to 0, -1 to 1, 0 to -1, 0 to 1, 1 to -1, 1 to 0, 1 to 1)
            .sumOf { dir ->
                var c = coord + dir
                var n = 0L
                while (
                    c.x in 0 until (input.boardSize) &&
                    c.y in 0 until (input.boardSize) &&
                    LosCoord(c.x, c.y) !in input.obstacles
                ) {
                    n++
                    c += dir
                }
                n
            }

    }
}

@Serializable
data class LineOfSightInput(val boardSize: Int, val obstacles: List<LosCoord>)

@Serializable
data class LosCoord(val x: Int, val y: Int)
