package com.szendo.snapchallenge.solver

object MadDoctorProblem : Problem<List<Int>, Long>("mad-doctor") {
    override fun solve(input: List<Int>): Long {
        val food = MutableList(input.size) { 1 }
        var index = 1
        while (index <= input.lastIndex) {
            if (input[index] > input[index - 1]) {
                food[index] = food[index - 1] + 1
            }
            index++
        }
        index = input.lastIndex
        while (index >= 1) {
            if (input[index - 1] > input[index]) {
                food[index - 1] = maxOf(food[index - 1], food[index] + 1)
            }
            index--
        }
        return food.sumOf { it.toLong() }
    }
}
