package com.szendo.snapchallenge.solver

abstract class Problem<In, Out>(val problemId: String) {
    abstract fun solve(input: In): Out
}
