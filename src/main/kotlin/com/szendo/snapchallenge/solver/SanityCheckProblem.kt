package com.szendo.snapchallenge.solver

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

object SanityCheckProblem : Problem<SanityCheckInput, SanityCheckOutput>("sanity-check") {
    override fun solve(input: SanityCheckInput): SanityCheckOutput {
        val maxOneBits = input.set.maxOf { it.countOneBits() }
        val insaneNumbers = input.set.filter { it.countOneBits() == maxOneBits }
        return SanityCheckOutput(insaneNumbers)
    }

}

@Serializable
data class SanityCheckInput(
    val meta: Meta,
    val set: List<ULong>,
) {
    @Serializable
    data class Meta(@SerialName("set_length") val setLength: Int)
}

@Serializable
data class SanityCheckOutput(
    @SerialName("insane_numbers") val insaneNumbers: List<ULong>,
)
