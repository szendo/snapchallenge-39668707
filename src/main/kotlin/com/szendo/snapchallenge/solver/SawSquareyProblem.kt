package com.szendo.snapchallenge.solver

import kotlinx.serialization.Serializable
import kotlin.math.abs

object SawSquareyProblem : Problem<SawSquareyInput, Int>("saw-squarey") {
    private const val N = 3
    private const val MAGIC = N * (N * N + 1) / 2

    override fun solve(input: SawSquareyInput) = allMagicSquares.minOf { calculateCost(it, input.square) }

    operator fun <T> List<MutableList<T>>.set(coord: Coord, value: T) = this[coord.y].set(coord.x, value)

    private val allMagicSquares =
        (0 until N).flatMap { startY ->
            (0 until N).flatMap { startX ->
                setOf(0 to -1, 1 to 0, 0 to 1, -1 to 0).flatMap { majorDir ->
                    listOf(
                        generateMagicSquare(startX to startY, majorDir, -majorDir.y to majorDir.x),
                        generateMagicSquare(startX to startY, majorDir, majorDir.y to -majorDir.x),
                    )
                }
            }
        }.filter { isValidMagicSquare(it) }.toSet()

    private fun generateMagicSquare(start: Coord, majorDir: Coord, minorDir: Coord): Grid<Int> {
        val s = List(N) { MutableList(N) { 0 } }
        var pos = start
        (1..9).forEach { n ->
            s[pos] = n
            pos = (if (n % N == 0) pos - majorDir else pos + majorDir + minorDir) mod N
        }
        return s
    }

    private fun isValidMagicSquare(grid: Grid<Int>): Boolean =
        (0 until N).all { x -> (0 until N).sumOf { y -> grid[x to y] } == MAGIC } &&
                (0 until N).all { y -> ((0 until N).sumOf { x -> grid[x to y] } == MAGIC) } &&
                (0 until N).sumOf { x -> grid[x to x] } == MAGIC &&
                (0 until N).sumOf { x -> grid[x to N - x - 1] } == MAGIC

    private fun calculateCost(grid1: Grid<Int>, grid2: Grid<Int>) =
        (0 until N).sumOf { y ->
            (0 until N).sumOf { x -> abs(grid1[x to y] - grid2[x to y]) }
        }
}

@Serializable
data class SawSquareyInput(val square: Grid<Int>)
