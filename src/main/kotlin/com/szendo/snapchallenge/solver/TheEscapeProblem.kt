package com.szendo.snapchallenge.solver

import kotlinx.serialization.Serializable
import java.util.*

object TheEscapeProblem : Problem<Grid<Char>, List<MazeCoord>>("the-escape") {
    override fun solve(input: Grid<Char>): List<MazeCoord> {
        val startCoord = input.findFirst('S')
        val keyCoord = input.findFirst('K')
        val endCoord = input.findFirst('E')
        val getNeighbors = { coord: Coord ->
            listOf(coord + (1 to 0), coord + (0 to 1), coord + (-1 to 0), coord + (0 to -1))
                .filter { (input.getOrNull(it) ?: '#') != '#' }
                .toSet()
        }
        val pathToKey = shortestPathBfs(startCoord, keyCoord, getNeighbors)
        val pathToEnd = shortestPathBfs(keyCoord, endCoord, getNeighbors)
        return (pathToKey + pathToEnd.drop(1)).map { MazeCoord(it.x, it.y) }
    }
}

@Serializable
data class MazeCoord(val x: Int, val y: Int)

inline fun <S> shortestPathBfs(
    source: S, target: S,
    crossinline getNeighbors: (s: S) -> Set<S>,
): List<S> {
    val queue = LinkedList(listOf(source to listOf(source)))
    val visited = mutableSetOf<S>()
    while (queue.isNotEmpty()) {
        val (state, path) = queue.removeFirst()
        if (state == target) return path
        if (visited.add(state)) {
            getNeighbors(state).forEach {
                queue.add(it to path + it)
            }
        }
    }
    error("No path to target")
}
