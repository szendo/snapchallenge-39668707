package com.szendo.snapchallenge.solver

typealias Coord = Pair<Int, Int>

inline val Coord.x: Int get() = first
inline val Coord.y: Int get() = second
operator fun Coord.plus(other: Coord) = (x + other.x) to (y + other.y)
operator fun Coord.minus(other: Coord) = (x - other.x) to (y - other.y)
infix fun Coord.mod(n: Int) = Math.floorMod(this.x, n) to Math.floorMod(this.y, n)

typealias Grid<T> = List<List<T>>

operator fun <T> Grid<T>.get(coord: Coord) = this[coord.y][coord.x]
fun <T> Grid<T>.getOrNull(coord: Coord) = this.getOrNull(coord.y)?.getOrNull(coord.x)
fun <T> Grid<T>.findFirst(e: T) = this.indexOfFirst { it.contains(e) }.let { this[it].indexOf(e) to it }
