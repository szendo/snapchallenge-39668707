package com.szendo.snapchallenge

import com.szendo.snapchallenge.solver.TheEscapeProblem

fun main() {
    ChallengeRunner(System.getenv("API_TOKEN")).run(sampleIndex = 0, TheEscapeProblem)
}
