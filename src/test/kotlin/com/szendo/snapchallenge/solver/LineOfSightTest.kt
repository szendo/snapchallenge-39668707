package com.szendo.snapchallenge.solver

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertContains

internal class LineOfSightTest {
    @ParameterizedTest(name = "{0} → any of {1}")
    @MethodSource("getSample0")
    fun solveSamples(inputStr: String, expectedOutputsStrs: Set<String>) {
        val input = Json.decodeFromString<LineOfSightInput>(inputStr)
        val expectedOutputs = expectedOutputsStrs.map { Json.decodeFromString<LosCoord>(it) }.toSet()
        assertContains(expectedOutputs, LineOfSightProblem.solve(input))
    }

    companion object {
        @JvmStatic
        fun getSample0(): List<Arguments> =
            listOf(
                Arguments.of(
                    """{"boardSize":4,"obstacles":[{"x":3,"y":2},{"x":0,"y":0},{"x":1,"y":3},{"x":2,"y":1}]}""",
                    setOf(
                        """{"x":1,"y":1}""",
                        """{"x":1,"y":2}""",
                    )
                ),
                Arguments.of(
                    """{"boardSize":5,"obstacles":[{"x":4,"y":3},{"x":0,"y":1},{"x":3,"y":4},{"x":1,"y":2},{"x":3,"y":0}]}""",
                    setOf(
                        """{"x":2,"y":2}""",
                    ),
                ),
                Arguments.of(
                    """{"boardSize":8,"obstacles":[{"x":4,"y":1},{"x":3,"y":4},{"x":4,"y":7},{"x":4,"y":5},{"x":0,"y":3},{"x":2,"y":0},{"x":7,"y":6},{"x":5,"y":2}]}""",
                    setOf(
                        """{"x":1,"y":3}""",
                        """{"x":3,"y":3}""",
                        """{"x":6,"y":6}""",
                    ),
                ),
                Arguments.of(
                    """{"boardSize":10,"obstacles":[{"x":0,"y":1},{"x":0,"y":2},{"x":0,"y":6},{"x":0,"y":7},{"x":0,"y":8},{"x":1,"y":1},{"x":1,"y":6},{"x":1,"y":7},{"x":2,"y":4},{"x":2,"y":7},{"x":2,"y":9},{"x":3,"y":6},{"x":3,"y":8},{"x":3,"y":9},{"x":4,"y":4},{"x":4,"y":5},{"x":4,"y":6},{"x":4,"y":9},{"x":5,"y":2},{"x":5,"y":3},{"x":5,"y":5},{"x":5,"y":7},{"x":5,"y":8},{"x":6,"y":1},{"x":6,"y":5},{"x":6,"y":6},{"x":6,"y":9},{"x":7,"y":2},{"x":7,"y":3},{"x":7,"y":4},{"x":7,"y":5},{"x":7,"y":6},{"x":7,"y":8},{"x":8,"y":1},{"x":8,"y":4},{"x":9,"y":3},{"x":9,"y":4},{"x":9,"y":6},{"x":9,"y":8},{"x":9,"y":9}]}""",
                    setOf(
                        """{"x":3,"y":0}""",
                        """{"x":4,"y":0}""",
                    )
                ),
            )
    }
}
