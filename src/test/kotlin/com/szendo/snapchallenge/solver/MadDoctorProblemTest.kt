package com.szendo.snapchallenge.solver

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

internal class MadDoctorProblemTest {
    @Test
    fun solveSample() {
        val input = Json.decodeFromString<List<Int>>("[1, 1, 2, 3, 3, 3, 2, 10]")
        val expectedOutput = Json.decodeFromString<Long>("13")
        assertEquals(expectedOutput, MadDoctorProblem.solve(input))
    }
}
