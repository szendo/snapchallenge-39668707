package com.szendo.snapchallenge.solver

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

internal class SanityCheckProblemTest {
    @Test
    fun solveSample() {
        val input = Json.decodeFromString<SanityCheckInput>(
            """
                {
                    "meta": {
                      "set_length": 4
                    },
                    "set": [
                        1,
                        233,
                        100,
                        64
                    ]
                }
            """.trimIndent()
        )
        val expectedOutput = Json.decodeFromString<SanityCheckOutput>(
            """
                {
                    "insane_numbers": [
                      233
                    ]
                }
            """.trimIndent()
        )
        assertEquals(expectedOutput, SanityCheckProblem.solve(input))
    }
}
