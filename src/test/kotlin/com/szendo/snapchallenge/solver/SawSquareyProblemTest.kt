package com.szendo.snapchallenge.solver

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals

internal class SawSquareyProblemTest {
    @ParameterizedTest(name = "{0} → {1}")
    @MethodSource("getSample0")
    fun solveSample(inputStr: String, expectedOutputStr: String) {
        val input = Json.decodeFromString<SawSquareyInput>(inputStr)
        val expectedOutput = Json.decodeFromString<Int>(expectedOutputStr)
        assertEquals(expectedOutput, SawSquareyProblem.solve(input))
    }

    companion object {
        @JvmStatic
        fun getSample0(): List<Arguments> =
            listOf(
                Arguments.of(
                    """{"square":[[8,3,5],[1,5,9],[4,7,1]]}""",
                    """4""",
                ),
                Arguments.of(
                    """{"square":[[3,5,2],[1,5,3],[6,7,4]]}""",
                    """17""",
                ),
                Arguments.of(
                    """{"square":[[5,1,6],[1,8,7],[4,2,2]]}""",
                    """15""",
                ),
                Arguments.of(
                    """{"square":[[5,4,9],[8,2,1],[7,6,4]]}""",
                    """15""",
                ),
                Arguments.of(
                    """{"square":[[5,4,8],[7,2,1],[6,5,3]]}""",
                    """16""",
                ),
                Arguments.of(
                    """{"square":[[8,2,2],[2,7,1],[1,7,1]]}""",
                    """20""",
                ),
                Arguments.of(
                    """{"square":[[3,7,6],[5,6,1],[4,8,8]]}""",
                    """11""",
                ),
            )
    }
}
