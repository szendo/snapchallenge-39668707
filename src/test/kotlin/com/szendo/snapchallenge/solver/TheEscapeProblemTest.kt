package com.szendo.snapchallenge.solver

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

internal class TheEscapeProblemTest {
    @Test
    fun solveSample() {
        val input = Json.decodeFromString<Grid<Char>>(
            """
                [
                  ["#", "E", "#", "#", "#"],
                  ["#", ".", "#", "K", "#"],
                  ["#", ".", "#", ".", "#"],
                  ["#", ".", ".", ".", "#"],
                  ["#", "S", "#", "#", "#"]
                ]
            """.trimIndent()
        )
        val expectedOutput = Json.decodeFromString<List<MazeCoord>>(
            """
                [
                  { "x": 1, "y": 4 },
                  { "x": 1, "y": 3 },
                  { "x": 2, "y": 3 },
                  { "x": 3, "y": 3 },
                  { "x": 3, "y": 2 },
                  { "x": 3, "y": 1 },
                  { "x": 3, "y": 2 },
                  { "x": 3, "y": 3 },
                  { "x": 2, "y": 3 },
                  { "x": 1, "y": 3 },
                  { "x": 1, "y": 2 },
                  { "x": 1, "y": 1 },
                  { "x": 1, "y": 0 }
                ]
            """.trimIndent()
        )
        assertEquals(expectedOutput, TheEscapeProblem.solve(input))
    }
}
